import fs from "fs";
import { join } from "path";

import pug from "pug";
import glob from "glob";

import { defineConfig } from "vite"
import pugPlugin from "vite-plugin-pug"


const options = { pretty: true } // FIXME: pug pretty is deprecated!
const locals = { name: "My Pug" }

const inputGeneratedPage = [];
const headerItems = JSON.parse(fs.readFileSync(join(__dirname, 'src', "data", fs.readdirSync(join(__dirname, "src", "data")).find(dataFile => dataFile === "headerItems.json")), "utf8"));

const compileAritcles = fs.readdirSync(join(__dirname, "src", "data")).forEach(data => {
    if(data.split("_")[0] === "article") {
        const articleData = JSON.parse(fs.readFileSync(join(__dirname, "src", "data", data), 'utf-8'));
        const articlePageTemplate = fs.readFileSync(join(__dirname, "src", "components", "articlePage.pug"), "utf-8");
        const compileAritcle = pug.compileFile(
            join(__dirname, "src", "components", "articlePage.pug"), 
            { 
                pretty: true,
                locals
            }
        )(Object.assign(articleData, { header: headerItems }));
        
        const generatedName = data.split("_")[1].split(".")[0];
        console.log(generatedName, "test");
        try {
            fs.readdirSync("dist")  
        } catch(err) {
            return fs.mkdirSync("dist");
        }

        fs.writeFileSync(`./dist/${generatedName}.html`.trim(), compileAritcle);
        fs.writeFileSync(`${generatedName}.html`.trim(), compileAritcle);
        inputGeneratedPage.push({ [articleData.title]:`./${articleData.title}.html`.trim()});
    }
});

const dataForPages = {};
const findAllData = fs.readdirSync(join(__dirname, "src", "data"));
const articles = fs.readdirSync(join(__dirname, "src", "data")).filter(dataFile => dataFile.match(/article/));
const articlesData = [];
articles.forEach(articleData => {
    const data = JSON.parse(fs.readFileSync(join(__dirname, "src", "data", articleData), 'utf-8'));
    articlesData.push(articleData.split("_")[1].split(".")[0]);
});

console.log(articlesData);

findAllData.forEach(dataPages => {
    const data = JSON.parse(fs.readFileSync(join(__dirname, "src", "data", dataPages), 'utf-8'));
    const pagesName = dataPages.split(".")[0];
    dataForPages[pagesName] = Object.assign(data, { articles: articlesData });
    console.log(dataPages, pagesName, "test1");
});
console.log(dataForPages);

export default defineConfig({
    assetsInclude: ['./'],
    plugins: [
        pugPlugin(options, dataForPages)
    ],
    build: {    
        rollupOptions: {
            input: {
                index: "./index.html",
                timeline: "./timeLine.html"
            }
        }
    }
})