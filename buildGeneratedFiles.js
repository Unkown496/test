import fs from "fs";
import { join, dirname } from "path";
import { fileURLToPath } from 'url';

import pug from "pug";

const __filename = fileURLToPath(import.meta.url);

// 👇️ "/home/john/Desktop/javascript"
const __dirname = dirname(__filename);

const inputGeneratedPage = [];
const headerItems = JSON.parse(fs.readFileSync(join(__dirname, 'src', "data", fs.readdirSync(join(__dirname, "src", "data")).find(dataFile => dataFile === "headerItems.json")), "utf8"));
const locals = "";

const compileAritcles = fs.readdirSync(join(__dirname, "src", "data")).forEach(data => {
    if(data.split("_")[0] === "article") {
        const articleData = JSON.parse(fs.readFileSync(join(__dirname, "src", "data", data), 'utf-8'));
        const articlePageTemplate = fs.readFileSync(join(__dirname, "src", "components", "articlePage.pug"), "utf-8");
        const compileAritcle = pug.compileFile(
            join(__dirname, "src", "components", "articlePage.pug"), 
            { 
                pretty: true,
                locals
            }
        )(Object.assign(articleData, { header: headerItems }));
        
        const generatedName = data.split("_")[1].split(".")[0];
        console.log(generatedName, "test");
        try {
            fs.readdirSync("dist")  
        } catch(err) {
            return fs.mkdirSync("dist");
        }

        fs.writeFileSync(`./dist/${generatedName}.html`.trim(), compileAritcle);
        inputGeneratedPage.push({ [articleData.title]:`./${articleData.title}.html`.trim()});
    }
});